import short_url
from flask import Flask, request, render_template, make_response, g, redirect
from sqlalchemy import *
import datetime
import re

app = Flask(__name__)

# pagination items per page
app.PAGE_LIMIT = 15


def filt(string):
    """ cut unsafe characters """
    chars = [',', '!', '.', ';', '?', '<', '>', '/']
    string = re.sub('[%s]' % ''.join(chars), '', string)
    return string


@app.errorhandler(404)
def page(e):
    """ Redirect """
    conn = db()
    string = filt(request.path)
    query = """
            SELECT (link_url) FROM links WHERE link_url_short = '%s'
            """ % string
    res = conn.execute(query)
    
    if res.rowcount > 0:
        res = res.fetchall()
        link = str(res[0][0])
        # increase counter
        query = """
                UPDATE links SET link_count = link_count + 1 WHERE link_url_short = '%s'
                """ % string
        conn.execute(query)
        db_close()
        return redirect(link)
    else:
        db_close()
        return render_template('error.html', error='Link not exist or expired')


@app.route('/')
def index():
    """ Index page """
    
    conn = db()
    
    query = """
            SELECT * FROM links ORDER BY link_date DESC LIMIT %s
            """ % app.PAGE_LIMIT
    res = conn.execute(query)
    arr = []
    for x in res:
        arr.append(x[4])
    db_close()
    return render_template('index.html', tops=arr)


@app.route('/edit', methods=['GET'])
def edit():
    """ Link editor """
    page = filt(request.args.get('page', ''))
    if page == '':
        page = 0
    page = int(page)
    conn = db()
    
    # total items count
    query = """
            SELECT COUNT(link_id) FROM links
            """
    res = conn.execute(query)
    fetch = res.fetchall()
    total_items = int(fetch[0][0])
    
    # calculate pagination
    limit = app.PAGE_LIMIT
    total_pages = int(total_items / limit)
    offset = int((page / total_pages) * limit)
    start = int(page / limit)
    
    query = """
            SELECT * FROM links LIMIT %(pg_limit)s OFFSET %(pg_offset)s
            """ % {'pg_limit': app.PAGE_LIMIT, 'pg_offset': offset}
    res = conn.execute(query)
    
    if res.rowcount > 0:
        out = []
        for x in res:
            _out = {}
            _out['link_id'] = x['link_id']
            _out['link_url'] = x['link_url']
            _out['link_date'] = x['link_date']
            _out['link_url_short'] = x['link_url_short']
            out.append(_out)
        
        return render_template(
            'edit.html', 
            results = out,
            total_pgs = total_pages,
            current_pg = page,
            offset_pg = offset,
            start_pg = start
        )
    else:
        return render_template('error.html', error='No results found')
    return 'Page number: %s' % res.rowcount


@app.route('/new', methods=['POST'])
def new():
    """ New url 
    CREATE SEQUENCE links_id START 1;
    """
    url = request.form['url']
    
    if validateUrl(url) == None:
        return render_template('error.html', error='Not a link !!!')
    
    conn = db()
    
    query = """
            SELECT COUNT(link_id) FROM links
            """
    res = conn.execute(query)
    fetch = res.fetchall()
    count = int(fetch[0][0]) + 1
    short = short_url.encode_url(count)
    date = datetime.datetime.now()
    
    query = """
            INSERT INTO links (link_id, link_url, link_url_short, link_date, link_count)
            VALUES (nextval('links_id'), '%(url)s', '%(short)s', '%(date)s', 0)
            """ % {'url': url, 'date': date, 'short': short}
    res = conn.execute(query)
    
    db_close()
    
    return render_template('new_link.html', link=short)


@app.route('/edit', methods=['GET'])
def delete():
    item = filt(request.args.get('item', ''))
    return 'Deleted item: %s' % item


def db():
    """ db connection """
    conn = getattr(g, 'database_connection', None)
    if conn is None:
        e = create_engine('postgresql+psycopg2://postgres:vusez7wehar@localhost/site')
        g.conn = conn = e.connect()
    return conn


def db_close():
    """ close db connection """
    g.conn.close()
    return


def validateUrl(string):
    """ validate url """
    regex = re.compile(
            r'^(?:http|ftp)s?://'
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
            r'localhost|'
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
            r'(?::\d+)?'
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return regex.match(string)


if __name__ == '__main__':
    app.run(debug=True)